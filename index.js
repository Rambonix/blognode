var express = require("express"),
    handlebars = require('express-handlebars');
	bodyParser = require("body-parser");
	
var app = express();
 
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            },
            {
                "subject": "Post numero 2",
                "description": "Descripcion 2",
                "time": new Date()
            },
            {
                "subject": "Post numero 3",
                "description": "Descripcion 3",
                "time": new Date()
            }
            ];
			
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())
 
app.engine('handlebars', handlebars());
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 
app.get('/posts', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});


app.get('/posts/del', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});

app.get('/posts/new', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : [posts[posts.length - 1]] } );
});

app.get('/posts/:id', function(req, res, next){
	var id = req.params.id;
    res.render('posts', { "title": "Hola gente", "posts" : [posts[id]] } );
});

app.get('/posts/edit/:id', function(req, res){
    var id = req.params.id;
	var edited = "@VAPORCATSTUDIO ";
	for(var i = 0; i < posts.length; i++){
		if(i == id){
			posts[id].subject = edited;
			posts[id].description = edited;
			posts[id].time = edited;
		}
	}
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});

app.post('/posts', function(req, res){
	
	posts.push(req.body);

    res.end(JSON.stringify(req.body));
});

app.put('/posts/:id', function(req, res){
    var id = req.params.id;
	var edited = "@VAPORCATSTUDIO ";
	for(var i = 0; i < posts.length; i++){
		if(i == id){
			posts[id].subject = edited;
			posts[id].description = edited;
			posts[id].time = edited;
		}
	}
    res.render('posts', { "title": "Hola gente", "posts" : posts } );
});


app.get('/posts/delete/:id', function(req, res){
	var id = req.params.id;
	posts.splice(id, 1);
    res.render('posts', { "title": "Hola gente ", "posts" : posts } );
});



app.listen(8080);